
DESCRIPTION
-----------

Simplenews To Dolist synchronizes dolist email addresses with simplenews email addresses.

The module :
- reads all email adresses in simplenews tables (in all categories if several exist)
- reads all email adresses in dolist database
- if a simplenews address doesn't exist in dolist, the address is added and validated in dolist
- if a dolist address doesn't exist in simplenews, the address is disactivated in dolist
- if an email address exists both in simplenews and dolist, email address is automatically activated in dolist

REQUIREMENTS
------------

 * For large mailing lists, cron is required.
 

INSTALLATION
------------

 1. CREATE DIRECTORY

    Create a new directory "simplenews_to_dolist" in the sites/all/modules directory and
    place the entire contents of this simplenews_to_dolist folder in it.

 2. ENABLE THE MODULE

    Enable the module on the Modules admin page.
	
	You need also to download and enable 
	- the module simplenews : http://http://drupal.org/project/simplenews

 3. CONFIGURE SIMPLENEWS_TO_DOLIST

    Configure Dolist AccountId and Api key at :
      /administration/simple_dolist_admin

 4. SYNCHRONIZE DOLIST
 
    The Url to synchronize Dolist database is at : /administration/simple_dolist_synchro
	
 5. LOG
 
	A log is visible in dblog at : /admin/reports/dblog
	where you can see error messages and reports, with type : 'cron simple_to_dolist synchro'


RELATED MODULES
------------

 * Simplenews
   http://http://drupal.org/project/simplenews


